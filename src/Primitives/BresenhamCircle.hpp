#ifndef BRESENHAMCIRCLE_HPP
#define BRESENHAMCIRCLE_HPP

#include "../Bitmap/Bitmap.hpp"

void BresenhamCircle(Bitmap& bitmap, int center_x, int center_y, int radius) {
  int x = 0;
  int y = radius;

  int delta_big = 1 - radius;

  while (x <= y) {
    bitmap.SetPixel(center_x + x, center_y + y, 0, 0, 255, 255);
    bitmap.SetPixel(center_x + x, center_y - y, 0, 0, 255, 255);
    bitmap.SetPixel(center_x + y, center_y + x, 0, 0, 255, 255);
    bitmap.SetPixel(center_x + y, center_y - x, 0, 0, 255, 255);

    bitmap.SetPixel(center_x - x, center_y + y, 0, 0, 255, 255);
    bitmap.SetPixel(center_x - x, center_y - y, 0, 0, 255, 255);
    bitmap.SetPixel(center_x - y, center_y + x, 0, 0, 255, 255);
    bitmap.SetPixel(center_x - y, center_y - x, 0, 0, 255, 255);

    if (delta_big <= 0) {
      delta_big += 2 * x + 1;
    } else {
      delta_big += 2 * x + 3 - 2 * y;
      --y;
    }
    ++x;
  }
}

#endif  // BRESENHAMCIRCLE_HPP
