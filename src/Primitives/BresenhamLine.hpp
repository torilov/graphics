#ifndef BRESENHAMLINE_HPP
#define BRESENHAMLINE_HPP

#include "../Bitmap/Bitmap.hpp"

void BresenhamLine(Bitmap& bitmap, int line_start_x, int line_start_y,
                   int line_end_x, int line_end_y, unsigned char red = 0,
                   unsigned char green = 0, unsigned char blue = 255,
                   unsigned char alpha = 255) {
  int x = line_start_x;
  int y = line_start_y;

  int delta_x = std::abs(line_end_x - line_start_x);
  int delta_y = std::abs(line_end_y - line_start_y);

  const int s1 = (line_end_x - line_start_x) > 0 ? 1 : -1;
  const int s2 = (line_end_y - line_start_y) > 0 ? 1 : -1;

  bool changed = false;

  if (delta_y > delta_x) {
    changed = true;
    std::swap(delta_x, delta_y);
  }

  int err = 2 * delta_y - delta_x;

  int i = 1;
  while (i <= delta_x) {
    bitmap.SetPixel(x, y, red, green, blue, alpha);
  m:
    if (err > 0) {
      if (changed) {
        x += s1;
      } else {
        y += s2;
      }
      err -= 2 * delta_x;
      goto m;
    } else {
      if (changed) {
        y += s2;
      } else {
        x += s1;
      }

      err += 2 * delta_y;
      ++i;
    }
  }
}

#endif  // BRESENHAMLINE_HPP
