#ifndef BRESENHAMELLIPSE_HPP
#define BRESENHAMELLIPSE_HPP

#include "../Bitmap/Bitmap.hpp"

void BresenhamEllipse(Bitmap& bitmap, int center_x, int center_y, int radius_a,
                      int radius_b) {
  // math source: http://grafika.me/node/656
  int x = 0;
  int y = radius_b;

  int sqr_a = radius_a * radius_a;
  int sqr_b = radius_b * radius_b;

  int delta = 4 * sqr_b * ((x + 1) * (x + 1)) +
              sqr_a * ((2 * y - 1) * (2 * y - 1)) - 4 * sqr_a * sqr_b;

  while (sqr_a * (2 * y - 1) > 2 * sqr_b * (x + 1)) {
    bitmap.SetPixel(center_x + x, center_y + y, 0, 0, 255, 255);
    bitmap.SetPixel(center_x + x, center_y - y, 0, 0, 255, 255);
    bitmap.SetPixel(center_x - x, center_y - y, 0, 0, 255, 255);
    bitmap.SetPixel(center_x - x, center_y + y, 0, 0, 255, 255);

    if (delta < 0) {
      x++;
      delta += 4 * sqr_b * (2 * x + 3);
    } else {
      x++;
      delta += -8 * sqr_a * (y - 1) + 4 * sqr_b * (2 * x + 3);
      y--;
    }
  }

  delta = sqr_b * ((2 * x + 1) * (2 * x + 1)) +
          4 * sqr_a * ((y + 1) * (y + 1)) - 4 * sqr_a * sqr_b;
  while (y + 1 != 0) {
    bitmap.SetPixel(center_x + x, center_y + y, 0, 0, 255, 255);
    bitmap.SetPixel(center_x + x, center_y - y, 0, 0, 255, 255);
    bitmap.SetPixel(center_x - x, center_y - y, 0, 0, 255, 255);
    bitmap.SetPixel(center_x - x, center_y + y, 0, 0, 255, 255);

    if (delta < 0) {
      y--;
      delta += 4 * sqr_a * (2 * y + 3);
    } else {
      y--;
      delta += -8 * sqr_b * (x + 1) + 4 * sqr_a * (2 * y + 3);
      x++;
    }
  }
}
#endif  // BRESENHAMELLIPSE_HPP
