#ifndef COHENSUTHERLANDLINECLIPPING_HPP
#define COHENSUTHERLANDLINECLIPPING_HPP

#include "../Bitmap/Bitmap.hpp"
#include "../Primitives/BresenhamLine.hpp"

static int clip_x_min = 0;
static int clip_y_min = 0;
static int clip_x_max = 0;
static int clip_y_max = 0;
void LineClippingRegion(Bitmap& bitmap, int x1, int y1, int x2, int y2) {
  clip_x_min = std::min(x1, x2);
  clip_x_max = std::max(x1, x2);
  clip_y_min = std::min(y1, y2);
  clip_y_max = std::max(y1, y2);

  for (int i = clip_x_min; i <= clip_x_max; ++i) {
    bitmap.SetPixel(i, clip_y_min, 0, 0, 0, 255);
    bitmap.SetPixel(i, clip_y_max, 0, 0, 0, 255);
  }

  for (int i = clip_y_min; i <= clip_y_max; ++i) {
    bitmap.SetPixel(clip_x_min, i, 0, 0, 0, 255);
    bitmap.SetPixel(clip_x_max, i, 0, 0, 0, 255);
  }
}

// https://www.geeksforgeeks.org/line-clipping-set-1-cohen-sutherland-algorithm/
void LineClip(Bitmap& bitmap, int x1, int y1, int x2, int y2) {
  constexpr int inside = 0;
  constexpr int left = 1 << 0;
  constexpr int right = 1 << 1;
  constexpr int bottom = 1 << 2;
  constexpr int top = 1 << 3;

  auto checkPoint = [](double x, double y) -> int {
    int code = inside;

    if (x < clip_x_min)
      code |= left;
    else if (x > clip_x_max)
      code |= right;
    if (y < clip_y_min)
      code |= bottom;
    else if (y > clip_y_max)
      code |= top;

    return code;
  };

  auto findIntersection = [x1, y1, x2, y2](int code) -> std::pair<int, int> {
    if (code & top) {
      return {
          x1 + (x2 - x1) * (clip_y_max - y1) / (y2 - y1),
          clip_y_max,
      };
    }
    if (code & bottom) {
      return {
          x1 + (x2 - x1) * (clip_y_min - y1) / (y2 - y1),
          clip_y_min,
      };
    }
    if (code & right) {
      return {
          clip_x_max,
          y1 + (y2 - y1) * (clip_x_max - x1) / (x2 - x1),
      };
    }
    if (code & left) {
      return {
          clip_x_min,
          y1 + (y2 - y1) * (clip_x_min - x1) / (x2 - x1),
      };
    }
    throw std::invalid_argument("code must be outside of the region");
  };

  int code1 = checkPoint(x1, y1);
  int code2 = checkPoint(x2, y2);

  for (int i = 0; i < 4; ++i) {
    if (code1 == 0 && code2 == 0) {
      BresenhamLine(bitmap, x1, y1, x2, y2);
      return;
    }

    if (code1 & code2) {
      BresenhamLine(bitmap, x1, y1, x2, y2, 255, 0, 0);
      return;
    }

    if (code1 != 0) {
      auto [x, y] = findIntersection(code1);
      BresenhamLine(bitmap, x1, y1, x, y, 255, 0, 0);
      x1 = x;
      y1 = y;
      code1 = checkPoint(x1, y1);
    }

    if (code2 != 0) {
      auto [x, y] = findIntersection(code2);
      BresenhamLine(bitmap, x2, y2, x, y, 255, 0, 0);
      x2 = x;
      y2 = y;
      code2 = checkPoint(x2, y2);
    }
  }
}

#endif  // COHENSUTHERLANDLINECLIPPING_HPP
