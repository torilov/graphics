#ifndef CURVE_HPP
#define CURVE_HPP

#include "../Primitives/BresenhamCircle.hpp"
#include "../Primitives/BresenhamLine.hpp"
#include "./Strategies/ICurveStrategy.hpp"

class Curve {
 public:
  void Draw(Bitmap& bitmap,
            const std::vector<std::pair<double, double>>& points) const {
    if (this->strategy_ == nullptr) {
      throw std::runtime_error("strategy not defined");
    }

    this->markPoints(bitmap, points);

    auto curve = this->strategy_->CalculateCurve(points);
    for (size_t i = 0; i < curve.size(); ++i) {
      bitmap.SetPixel(static_cast<int>(curve[i].first),
                      static_cast<int>(curve[i].second), 255, 0, 255, 255);
    }
  }

  void SetStrategy(ICurveStrategy* strategy) {
    this->strategy_.reset(strategy);
  }

  bool point_mark_circle_enabled_ = true;
  int point_mark_circle_radius_ = 5;

 protected:
  void markPoints(Bitmap& bitmap,
                  const std::vector<std::pair<double, double>>& points) const {
    for (const auto& [x, y] : points) {
      if (this->point_mark_circle_enabled_) {
        BresenhamCircle(bitmap, static_cast<int>(x), static_cast<int>(y),
                        this->point_mark_circle_radius_);
      }
    }

    for (size_t i = 0; i < points.size() - 1; ++i) {
      BresenhamLine(bitmap, static_cast<int>(points[i].first),
                    static_cast<int>(points[i].second),
                    static_cast<int>(points[i + 1].first),
                    static_cast<int>(points[i + 1].second), 0, 0, 0, 255);
    }
  }

 private:
  std::unique_ptr<ICurveStrategy> strategy_;
};

#endif  // CURVE_HPP
