#ifndef COMPLEXDECASTELJAUBEZIERSTRATEGY_HPP
#define COMPLEXDECASTELJAUBEZIERSTRATEGY_HPP

#include <utility>
#include <vector>

#include "./DeCasteljauBezierStrategy.hpp"
#include "./ICurveStrategy.hpp"

class ComplexDeCasteljauStrategy : public DeCasteljauStrategy {
 public:
  ComplexDeCasteljauStrategy() = default;
  explicit ComplexDeCasteljauStrategy(int block_size)
      : block_size_(block_size){};

  [[nodiscard]] std::vector<std::pair<double, double>> CalculateCurve(
      const std::vector<std::pair<double, double>>& points) override {
    std::vector<std::pair<double, double>> result;
    std::vector<std::pair<double, double>> block;

    for (const auto& point : points) {
      block.push_back(point);

      if (block.size() == static_cast<size_t>(block_size_)) {
        auto buff = DeCasteljauStrategy::CalculateCurve(block);
        result.insert(result.end(), buff.begin(), buff.end());
        block.clear();
        block.push_back(point);
      }
    }

    if (!block.empty()) {
      auto buff = DeCasteljauStrategy::CalculateCurve(block);
      result.insert(result.end(), buff.begin(), buff.end());
    }

    return result;
  }

 private:
  int block_size_ = 3;
};

#endif  // COMPLEXDECASTELJAUBEZIERSTRATEGY_HPP
