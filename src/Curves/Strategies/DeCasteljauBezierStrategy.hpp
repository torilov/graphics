#ifndef DECASTELJAUBEZIERSTRATEGY_HPP
#define DECASTELJAUBEZIERSTRATEGY_HPP

#include "./ICurveStrategy.hpp"

class DeCasteljauStrategy : public ICurveStrategy {
 public:
  [[nodiscard]] std::vector<std::pair<double, double>> CalculateCurve(
      const std::vector<std::pair<double, double>>& points) override {
    std::vector<std::pair<double, double>> result;

    for (int i = 0; i < steps_; ++i) {
      double t = static_cast<double>(i) / this->steps_;
      result.push_back(calculatePoint(points, t));
    }

    return result;
  }

 protected:
  [[nodiscard]] static std::pair<double, double> calculatePoint(
      const std::vector<std::pair<double, double>>& points, double percent) {
    if (points.size() == 1) {
      return points[0];
    }

    std::vector<std::pair<double, double>> ps;

    for (size_t i = 0; i < points.size() - 1; ++i) {
      auto [x1, y1] = points[i];
      auto [x2, y2] = points[i + 1];

      auto point_percent = [](double a, double b, double p) -> double {
        return a + (b - a) * p;
      };

      ps.emplace_back(point_percent(x1, x2, percent),
                      point_percent(y1, y2, percent));
    }

    return calculatePoint(ps, percent);
  }

  int steps_ = 1000;
};

#endif  // DECASTELJAUBEZIERSTRATEGY_HPP
