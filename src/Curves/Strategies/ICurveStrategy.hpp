#ifndef ICURVESTRATEGY_HPP
#define ICURVESTRATEGY_HPP

#include <stdexcept>
#include <utility>
#include <vector>

class ICurveStrategy {
 public:
  [[nodiscard]] virtual std::vector<std::pair<double, double>> CalculateCurve(
      const std::vector<std::pair<double, double>>&) {
    throw std::logic_error("not implemented");
  }
  virtual ~ICurveStrategy() = default;
};

#endif  // ICURVESTRATEGY_HPP
