#ifndef COMPLEXDEBOORBSPLINESTRATEGY_HPP
#define COMPLEXDEBOORBSPLINESTRATEGY_HPP

// http://www.inf.ed.ac.uk/teaching/courses/cg/d3/deBoor.html

#include "DeBoorBSplineStrategy.hpp"

class ComplexDeBoorBSplineStrategy : public DeBoorBSplineStrategy {
 public:
  ComplexDeBoorBSplineStrategy() = default;
  explicit ComplexDeBoorBSplineStrategy(int block_size)
      : block_size_(block_size){};

  [[nodiscard]] std::vector<std::pair<double, double>> CalculateCurve(
      const std::vector<std::pair<double, double>>& points) override {
    std::vector<std::pair<double, double>> result;

    for (size_t step = 0; step < this->steps_; ++step) {
      for (size_t i = 0;
           i <= points.size() - static_cast<size_t>(this->block_size_); ++i) {
        auto block_begin_it = points.begin() + static_cast<long>(i);
        auto block_end_it = points.begin() + static_cast<long>(i) +
                            static_cast<long>(this->block_size_);
        std::vector<std::pair<double, double>> block(block_begin_it,
                                                     block_end_it);

        result.push_back(DeBoorBSplineStrategy::calculatePoint(block, step));
      }
    }

    return result;
  }

 private:
  int block_size_ = 3;
};

#endif  // COMPLEXDEBOORBSPLINESTRATEGY_HPP
