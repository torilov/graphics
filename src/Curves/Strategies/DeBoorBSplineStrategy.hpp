#ifndef DEBOORBSPLINESTRATEGY_HPP
#define DEBOORBSPLINESTRATEGY_HPP

// http://www.inf.ed.ac.uk/teaching/courses/cg/d3/deBoor.html

class DeBoorBSplineStrategy : public ICurveStrategy {
 public:
  [[nodiscard]] std::vector<std::pair<double, double>> CalculateCurve(
      const std::vector<std::pair<double, double>>& points) override {
    std::vector<std::pair<double, double>> result;
    result.reserve(this->steps_);

    for (size_t i = 0; i < this->steps_; ++i) {
      result.push_back(calculatePoint(points, i));
    }

    return result;
  }

 protected:
  [[nodiscard]] std::pair<double, double> calculatePoint(
      const std::vector<std::pair<double, double>>& points, size_t step) {
    double t = static_cast<double>(step) / static_cast<double>(this->steps_) +
               static_cast<double>(points.size()) - 1;

    std::pair<double, double> result{0, 0};

    for (size_t i = 0; i < points.size(); ++i) {
      double w = this->normalized(i + 1, points.size(), t);
      result.first += points[i].first * w;
      result.second += points[i].second * w;
    }

    return result;
  }

  [[nodiscard]] double normalized(size_t i, size_t k, double t) {
    if (k == 1) {
      if (t >= static_cast<double>(i - 1) && t < static_cast<double>(i)) {
        return 1;
      }
      return 0;
    }

    auto divide_if_denominator_is_not_zero_else_zero = [](double a,
                                                          double b) -> double {
      if (std::abs(b) < 1e-5) {
        return 0;
      }
      return a / b;
    };

    return divide_if_denominator_is_not_zero_else_zero(
               t - static_cast<double>(i - 1),
               static_cast<double>(i + k - 2) - static_cast<double>(i - 1)) *
               this->normalized(i, k - 1, t) +
           divide_if_denominator_is_not_zero_else_zero(
               static_cast<double>(i + k - 1) - t,
               static_cast<double>(i + k - 1) - static_cast<double>(i)) *
               this->normalized(i + 1, k - 1, t);
  }

  size_t steps_ = 1000;
};

#endif  // DEBOORBSPLINESTRATEGY_HPP
