#ifndef SIMPLE_BEZIER_STRATEGY_HPP
#define SIMPLE_BEZIER_STRATEGY_HPP

#include "./ICurveStrategy.hpp"

class MatrixBezierStrategy : public ICurveStrategy {
 public:
  [[nodiscard]] std::vector<std::pair<double, double>> CalculateCurve(
      const std::vector<std::pair<double, double>>& points) override {}
};

#endif  // SIMPLE_BEZIER_STRATEGY_HPP
