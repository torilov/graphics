#ifndef MATRIX_HPP
#define MATRIX_HPP

#include <array>

#include "./Vector.hpp"

template <size_t N>
class SquareMatrix {
 public:
  std::array<std::array<double, N>, N> arr_;

  [[nodiscard]] SquareMatrix() {
    std::transform(this->arr_.begin(), this->arr_.end(), this->arr_.begin(),
                   [](std::array<double, N> a) -> std::array<double, N> {
                     std::fill(a.begin(), a.end(), 0);
                     return a;
                   });
  };

  [[nodiscard]] explicit SquareMatrix(std::array<std::array<double, N>, N> a)
      : arr_(std::move(a)){};

  [[nodiscard]] SquareMatrix<N> operator*(const SquareMatrix<N>& other) const {
    SquareMatrix<N> result;
    for (size_t i = 0; i < N; ++i) {
      for (size_t j = 0; j < N; ++j) {
        for (size_t k = 0; k < N; ++k) {
          result.arr_[i][j] += this->arr_[i][k] * other.arr_[k][j];
        }
      }
    }
    return result;
  }

  [[nodiscard]] Vector<N> operator*(const Vector<N>& other) const {
    Vector<N> result;
    for (size_t i = 0; i < N; ++i) {
      for (size_t j = 0; j < N; ++j) {
        result.arr_[j] += this->arr_[i][j] * other.arr_[i];
      }
    }
    return result;
  }

  [[nodiscard]] SquareMatrix<N> operator+(const SquareMatrix<N>& other) const {
    SquareMatrix<N> result;
    for (size_t i = 0; i < N; ++i) {
      for (size_t j = 0; j < N; ++j) {
        result.arr_[i][j] += this->arr_[i][j] + other.arr_[i][j];
      }
    }
    return result;
  }

  [[nodiscard]] SquareMatrix<N> operator*=(const double d) {
    for (size_t i = 0; i < N; ++i) {
      for (size_t j = 0; j < N; ++j) {
        this->arr_[i][j] *= d;
      }
    }
  }

  [[nodiscard]] SquareMatrix<N> operator/=(const double d) {
    for (size_t i = 0; i < N; ++i) {
      for (size_t j = 0; j < N; ++j) {
        this->arr_[i][j] /= d;
      }
    }
  }
};

#endif  // MATRIX_HPP
