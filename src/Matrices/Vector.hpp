#ifndef VECTOR_HPP
#define VECTOR_HPP

#include <array>

template <size_t N>
class Vector {
 public:
  std::array<double, N> arr_;

  [[nodiscard]] Vector() {
    std::fill(this->arr_.begin(), this->arr_.end(), 0);
  };

  [[nodiscard]] explicit Vector(std::array<double, N> a) : arr_(a){};

  template <typename T>
  [[nodiscard]] explicit Vector(std::initializer_list<T> a) {
    if (a.size() != N) {
      throw std::invalid_argument("wrong argument length");
    }

    size_t i = 0;
    for (auto it : a) {
      this->arr_[i] = static_cast<double>(it);
      ++i;
    }
  }

  void operator*=(const double d) {
    for (size_t i = 0; i < N; ++i) {
      this->arr_[i] *= d;
    }
  }

  template <typename Q>
  void operator/=(const Q d) {
    for (size_t i = 0; i < N; ++i) {
      this->arr_[i] /= d;
    }
  }

  template <typename Q>
  [[nodiscard]] Vector<N> operator/(const Q other) const {
    Vector<N> result;
    for (size_t i = 0; i < N; ++i) {
      result[i] = this->arr_[i] / other;
    }
    return result;
  }

  template <typename Q>
  [[nodiscard]] Vector<N> operator*(const Q other) const {
    Vector<N> result;
    for (size_t i = 0; i < N; ++i) {
      result[i] = this->arr_[i] * other;
    }
    return result;
  }


  [[nodiscard]] Vector<N> operator-(const Vector<N> other) const {
    Vector<N> result;
    for (size_t i = 0; i < N; ++i) {
      result[i] = this->arr_[i] - other.arr_[i];
    }
    return result;
  }

  [[nodiscard]] Vector<N> operator+(const Vector<N> other) const {
    Vector<N> result;
    for (size_t i = 0; i < N; ++i) {
      result[i] = this->arr_[i] + other.arr_[i];
    }
    return result;
  }

  [[nodiscard]] double& operator[](const size_t index) {
    return this->arr_[index];
  }

  [[nodiscard]] const double& operator[](const size_t index) const {
    return this->arr_[index];
  }

  [[nodiscard]] static inline Vector<3> vectorProduct(const Vector<3>& a,
                                                      const Vector<3>& b) {
    return Vector<3>({
        a[1] * b[2] - b[1] * a[2],
        -(a[0] * b[2] - b[0] * a[2]),
        a[0] * b[1] - b[0] * a[1],
    });
  }

  [[nodiscard]] static inline double scalarProduct(const Vector<N>& a,
                                                   const Vector<N>& b) {
    double result = 0;
    for (size_t i = 0; i < N; ++i) {
      result += a[i] * b[i];
    }
    return result;
  }

  [[nodiscard]] inline double l2() {
    double result = 0;
    for (const auto& it : this->arr_) {
      result += it * it;
    }
    return std::sqrt(result);
  }

  // TODO(torilov): enable if N is big enough
  double x() const { return this->arr_[0]; }
  double y() const { return this->arr_[1]; }
  double z() const { return this->arr_[2]; }
};

#endif  // VECTOR_HPP
