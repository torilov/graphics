#ifndef RESIZES_HPP
#define RESIZES_HPP

#include "../Matrices/SquareMatrix.hpp"

template <typename T>
[[nodiscard]] T Resize(T value, double p, double q, double r, double s) {
  SquareMatrix<4> resize_matrix({{
      {p, 0, 0, 0},
      {0, q, 0, 0},
      {0, 0, r, 0},
      {0, 0, 0, s},
  }});

  return resize_matrix * value;
}

#endif  // RESIZES_HPP
