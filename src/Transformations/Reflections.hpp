#ifndef REFLECTIONS_HPP
#define REFLECTIONS_HPP

#include "../Matrices/SquareMatrix.hpp"

template <typename T>
[[nodiscard]] T ReflectXY(T value) {
  SquareMatrix<4> reflection_value({{
      {1, 0, 0, 0},
      {0, 1, 0, 0},
      {0, 0, -1, 0},
      {0, 0, 0, 1},
  }});

  return reflection_value * value;
}

template <typename T>
[[nodiscard]] T ReflectXZ(T value) {
  SquareMatrix<4> reflection_matrix({{
      {1, 0, 0, 0},
      {0, -1, 0, 0},
      {0, 0, 1, 0},
      {0, 0, 0, 1},
  }});

  return reflection_matrix * value;
}

template <typename T>
[[nodiscard]] T ReflectYZ(T value) {
  SquareMatrix<4> reflection_matrix({{
      {-1, 0, 0, 0},
      {0, 1, 0, 0},
      {0, 0, 1, 0},
      {0, 0, 0, 1},
  }});

  return reflection_matrix * value;
}

#endif  // REFLECTIONS_HPP
