#ifndef TRANSLATIONS_HPP
#define TRANSLATIONS_HPP

#include "../Matrices/SquareMatrix.hpp"
#include "../Matrices/Vector.hpp"

template <typename T>
[[nodiscard]] T Translate(T value, double x, double y, double z) {
  SquareMatrix<4> translation_matrix({{
      {1, 0, 0, 0},
      {0, 1, 0, 0},
      {0, 0, 1, 0},
      {x, y, z, 1},
  }});

  return translation_matrix * value;
}

template <typename T>
[[nodiscard]] T Translate(T value, Vector<3> coordinates) {
  SquareMatrix<4> translation_matrix({{
      {1, 0, 0, 0},
      {0, 1, 0, 0},
      {0, 0, 1, 0},
      {coordinates[0], coordinates[1], coordinates[2], 1},
  }});

  return translation_matrix * value;
}

#endif  // TRANSLATIONS_HPP
