#ifndef ROTATIONS_HPP
#define ROTATIONS_HPP

#include <cmath>

#include "../Matrices/SquareMatrix.hpp"

template <typename T>
[[nodiscard]] T RotateOx(T value, double angle) {
  using std::cos;
  using std::sin;

  SquareMatrix<4> rotation_matrix({{
      {1, 0, 0, 0},
      {0, cos(angle), sin(angle), 0},
      {0, -sin(angle), cos(angle), 0},
      {0, 0, 0, 1},
  }});

  return rotation_matrix * value;
}

template <typename T>
[[nodiscard]] T RotateOy(T value, double angle) {
  using std::cos;
  using std::sin;

  SquareMatrix<4> rotation_matrix({{
      {cos(angle), 0, -sin(angle), 0},
      {0, 1, 0, 0},
      {sin(angle), 0, cos(angle), 0},
      {0, 0, 0, 1},
  }});

  return rotation_matrix * value;
}

template <typename T>
[[nodiscard]] T RotateOz(T value, double angle) {
  using std::cos;
  using std::sin;

  SquareMatrix<4> rotation_matrix({{
      {cos(angle), sin(angle), 0, 0},
      {-sin(angle), cos(angle), 0, 0},
      {0, 0, 1, 0},
      {0, 0, 0, 1},
  }});

  return rotation_matrix * value;
}

#endif  // ROTATIONS_HPP
