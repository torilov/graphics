// Torilov Dmitry
// BSE 173
// Project 5.
// Linux, C++17, CMake, CONAN

#include <imgui-SFML.h>
#include <imgui.h>
#include <spdlog/spdlog.h>

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/System/Clock.hpp>
#include <SFML/Window/Event.hpp>
#include <cmath>
#include <tuple>
#include <vector>

#include "Bitmap/Bitmap.hpp"
#include "Curves/Curve.hpp"
#include "Curves/Strategies/ComplexDeBoorBSplineStrategy.hpp"
#include "Curves/Strategies/ComplexDeCasteljauBezierStrategy.hpp"
#include "Curves/Strategies/DeBoorBSplineStrategy.hpp"
#include "Curves/Strategies/DeCasteljauBezierStrategy.hpp"
#include "FloodFills/ScanlineFill.hpp"
#include "FloodFills/random_stuff.hpp"
#include "LineClipping/CohenSutherlandLineClipping.hpp"
#include "Matrices/SquareMatrix.hpp"
#include "Primitives/BresenhamCircle.hpp"
#include "Primitives/BresenhamEllipse.hpp"
#include "Primitives/BresenhamLine.hpp"

// Constant window_ shape
constexpr int width = 1920;
constexpr int height = 1080;

// ImGui interface
static const int items_number = 8;
static const char* items[items_number]{
    "Bresenham's Line",    "Bresenham's Circle",
    "Bresenham's Ellipse", "Scanline Fill",
    "Polygon Line",        "Line Clipping Region",
    "Line Clipping",       "Curves",
};
static int selected_item;

// Mouse
static bool draw_button_pressed;
static sf::Vector2i draw_coordinate_start;
static sf::Vector2i draw_coordinate_end;

int main() {
  std::vector<std::tuple<int, int, int, int>> lines;
  int block_size_casteljau = 3;
  int block_size_boor = 4;

  // Init spdlog
  spdlog::set_level(spdlog::level::debug);
  spdlog::info("Starting ImGui + SFML");

  // Main Window initial
  sf::RenderWindow window(sf::VideoMode(width, height), "SFML");
  window.setFramerateLimit(60);
  ImGui::SFML::Init(window);
  sf::Mouse::setPosition(sf::Vector2i(100, 200), window);

  Bitmap bitmap(width, height, &window);
  bitmap.ClearPixels();

  // ------------------------------- Main Loop --------------------------------
  sf::Clock deltaClock;
  while (window.isOpen()) {
    // Handle window_ close
    sf::Event event;
    while (window.pollEvent(event)) {
      ImGui::SFML::ProcessEvent(event);

      if (event.type == sf::Event::Closed) {
        window.close();
      }
    }

    // ----------------------- Handle mouse press -----------------------------
    if (sf::Mouse::isButtonPressed(sf::Mouse::Right)) {
      if (!draw_button_pressed) {
        draw_button_pressed = true;
        draw_coordinate_start = sf::Mouse::getPosition(window);
        spdlog::debug("start drawing {}", items[selected_item]);
      }
    } else {
      if (draw_button_pressed) {
        draw_coordinate_end = sf::Mouse::getPosition(window);

        int circle_center_x = draw_coordinate_start.x;
        int circle_center_y = draw_coordinate_start.y;

        int circle_radius = static_cast<int>(
            sqrt(std::pow(draw_coordinate_start.x - draw_coordinate_end.x, 2) +
                 std::pow(draw_coordinate_start.y - draw_coordinate_end.y, 2)));

        int ellipse_center_x =
            (draw_coordinate_start.x + draw_coordinate_end.x) / 2;
        int ellipse_center_y =
            (draw_coordinate_start.y + draw_coordinate_end.y) / 2;
        int ellipse_radius_a =
            std::abs((draw_coordinate_start.x - draw_coordinate_end.x)) / 2;
        int ellipse_radius_b =
            std::abs((draw_coordinate_start.y - draw_coordinate_end.y)) / 2;

        int floodfil_start_x =
            (draw_coordinate_start.x + draw_coordinate_end.x) / 2;
        int floodfil_start_y =
            (draw_coordinate_start.y + draw_coordinate_end.y) / 2;

        draw_button_pressed = false;
        spdlog::debug("end drawing {}", items[selected_item]);

        switch (selected_item) {
          case 0:
            BresenhamLine(bitmap, draw_coordinate_start.x,
                          draw_coordinate_start.y, draw_coordinate_end.x,
                          draw_coordinate_end.y);
            break;
          case 1:
            BresenhamCircle(bitmap, circle_center_x, circle_center_y,
                            circle_radius);
            break;
          case 2:
            BresenhamEllipse(bitmap, ellipse_center_x, ellipse_center_y,
                             ellipse_radius_a, ellipse_radius_b);
            break;
          case 3:
            ScanlineFill(bitmap, floodfil_start_x, floodfil_start_y);
            break;
          case 4:
            for (const auto& [x1, y1, x2, y2] : lines) {
              if (std::abs(draw_coordinate_start.x - x1) < 30 &&
                  std::abs(draw_coordinate_start.y - y1) < 30) {
                draw_coordinate_start.x = x1;
                draw_coordinate_start.y = y1;
              }
              if (std::abs(draw_coordinate_start.x - x2) < 30 &&
                  std::abs(draw_coordinate_start.y - y2) < 30) {
                draw_coordinate_start.x = x2;
                draw_coordinate_start.y = y2;
              }
              if (std::abs(draw_coordinate_end.x - x1) < 30 &&
                  std::abs(draw_coordinate_end.y - y1) < 30) {
                draw_coordinate_end.x = x1;
                draw_coordinate_end.y = y1;
              }
              if (std::abs(draw_coordinate_end.x - x2) < 30 &&
                  std::abs(draw_coordinate_start.y - y2) < 30) {
                draw_coordinate_end.x = x2;
                draw_coordinate_end.y = y2;
              }
            }
            BresenhamLine(bitmap, draw_coordinate_start.x,
                          draw_coordinate_start.y, draw_coordinate_end.x,
                          draw_coordinate_end.y);
            lines.push_back({draw_coordinate_start.x, draw_coordinate_start.y,
                             draw_coordinate_end.x, draw_coordinate_end.y});
            break;
          case 5:
            LineClippingRegion(bitmap, draw_coordinate_start.x,
                               draw_coordinate_start.y, draw_coordinate_end.x,
                               draw_coordinate_end.y);
            break;
          case 6:
            LineClip(bitmap, draw_coordinate_start.x, draw_coordinate_start.y,
                     draw_coordinate_end.x, draw_coordinate_end.y);
            break;
          case 7:
            for (const auto& [x1, y1, x2, y2] : lines) {
              if (std::abs(draw_coordinate_start.x - x1) < 30 &&
                  std::abs(draw_coordinate_start.y - y1) < 30) {
                draw_coordinate_start.x = x1;
                draw_coordinate_start.y = y1;
              }
              if (std::abs(draw_coordinate_start.x - x2) < 30 &&
                  std::abs(draw_coordinate_start.y - y2) < 30) {
                draw_coordinate_start.x = x2;
                draw_coordinate_start.y = y2;
              }
              if (std::abs(draw_coordinate_end.x - x1) < 30 &&
                  std::abs(draw_coordinate_end.y - y1) < 30) {
                draw_coordinate_end.x = x1;
                draw_coordinate_end.y = y1;
              }
              if (std::abs(draw_coordinate_end.x - x2) < 30 &&
                  std::abs(draw_coordinate_start.y - y2) < 30) {
                draw_coordinate_end.x = x2;
                draw_coordinate_end.y = y2;
              }
            }
            BresenhamLine(bitmap, draw_coordinate_start.x,
                          draw_coordinate_start.y, draw_coordinate_end.x,
                          draw_coordinate_end.y);
            lines.push_back({draw_coordinate_start.x, draw_coordinate_start.y,
                             draw_coordinate_end.x, draw_coordinate_end.y});
        }
      }
    }

    // ------------------------- Update menu window_ ---------------------------
    ImGui::SFML::Update(window, deltaClock.restart());

    ImGui::Begin("Control Panel");
    ImGui::SetWindowFontScale(2);
    ImGui::ListBox("Drawing", &selected_item, items, items_number);
    if (ImGui::Button("Clear")) {
      spdlog::debug("clear screen");
      bitmap.ClearPixels();
      lines.clear();
    }
    if (ImGui::Button("Polygon Fill")) {
      spdlog::debug("polygon fill");
      PolygonXORFill(bitmap, lines);
    }
    if (ImGui::Button("Polygon XOR Fill")) {
      spdlog::debug("polygon xor fill");
      PolygonXORFill(bitmap, lines);
    }

    auto lines_to_points = [](std::vector<std::tuple<int, int, int, int>> l)
        -> std::vector<std::pair<double, double>> {
      std::vector<std::pair<double, double>> points;
      points.reserve(l.size());
      for (auto& [x1, y1, x2, y2] : l) {
        points.emplace_back(static_cast<double>(x1), static_cast<double>(y1));
      }
      auto& [x1, y1, x2, y2] = l.back();
      points.emplace_back(static_cast<double>(x2), static_cast<double>(y2));
      return points;
    };

    if (ImGui::Button("De Casteljau")) {
      spdlog::debug("De Casteljau curve");
      Curve curve;
      curve.SetStrategy(new DeCasteljauStrategy());
      auto points = lines_to_points(lines);
      curve.Draw(bitmap, points);
    }

    ImGui::SliderInt("Complex De Casteljau block size", &block_size_casteljau,
                     1, 10);
    if (ImGui::Button("Complex De Casteljau")) {
      spdlog::debug("complex De Casteljau curve");
      Curve curve;
      curve.SetStrategy(new ComplexDeCasteljauStrategy(block_size_casteljau));
      auto points = lines_to_points(lines);
      curve.Draw(bitmap, points);
    }

    if (ImGui::Button("Closed Complex De Casteljau")) {
      spdlog::debug("closed complex De Casteljau curve");
      Curve curve;
      curve.SetStrategy(new ComplexDeCasteljauStrategy(block_size_casteljau));
      auto points = lines_to_points(lines);
      points.push_back(points.front());
      curve.Draw(bitmap, points);
    }

    if (ImGui::Button("De Boor")) {
      spdlog::debug("De Boor curve");
      Curve curve;
      curve.SetStrategy(new DeBoorBSplineStrategy());
      auto points = lines_to_points(lines);
      curve.Draw(bitmap, points);
    }

    ImGui::SliderInt("Complex De Boor block size", &block_size_boor, 1, 10);
    if (ImGui::Button("Complex De Boor")) {
      spdlog::debug("complex De Boor curve");
      Curve curve;
      curve.SetStrategy(new ComplexDeBoorBSplineStrategy(block_size_boor));
      auto points = lines_to_points(lines);
      curve.Draw(bitmap, points);
    }

    if (ImGui::Button("Closed Complex De Boor")) {
      spdlog::debug("closed complex De Boor curve");
      Curve curve;
      curve.SetStrategy(new ComplexDeBoorBSplineStrategy(block_size_boor));
      auto points = lines_to_points(lines);
      for (size_t i = 0; i < static_cast<size_t>(block_size_boor); ++i) {
        points.push_back(points[i]);
      }
      curve.Draw(bitmap, points);
    }

    if (ImGui::Button("About")) {
      spdlog::info(
          "C++/SFML/ImGui app. Build with CMake and CONAN. "
          "Written by Torilov Dmitry. "
          "Uses initial C++ repo by Jason Turner.");
    }
    if (ImGui::Button("Description")) {
      spdlog::info(
          "Use Mouse to draw lines, circles and ellipses. "
          "You can also flood fill by selecting corresponding tool and "
          "pressing the mouse button. "
          "You can draw curves by selecting curve drawer in menu and then "
          "pressing needed curve's button. "
          "Line clipping works by using the needed tools: line clipping region "
          "and special lines.");
    }
    ImGui::End();

    bitmap.Update();

    ImGui::SFML::Render(window);
    window.display();
  }

  ImGui::SFML::Shutdown();

  return 0;
}
