#ifndef SCANLINEFILL_HPP
#define SCANLINEFILL_HPP

#include <stack>
#include <utility>

#include "../Bitmap/Bitmap.hpp"

void ScanlineFill(Bitmap& bitmap, int start_x, int start_y) {
  std::stack<std::pair<int, int>> s;
  s.push({start_x, start_y});

  const auto wall = std::make_tuple(0, 0, 255, 255);
  const auto fill_color = std::make_tuple(125, 197, 226, 255);

  while (!s.empty()) {
    const auto [x, y] = s.top();
    s.pop();

    // go right
    int x_max;
    for (x_max = x; x_max < bitmap.GetWidth(); ++x_max) {
      const auto pixel = bitmap.GetPixel(x_max, y);
      if (pixel == wall) {
        break;
      }
    }
    --x_max;

    // fill left
    int x_min = 0;
    int i;
    for (i = x_max; i > 0; --i) {
      x_min = i;
      const auto pixel = bitmap.GetPixel(i, y);
      if (pixel == wall) {
        break;
      }
      bitmap.SetPixel(i, y, 125, 197, 226, 255);
    }

    // find top switches
    bool first = true;
    if (y - 1 >= 0) {
      for (i = x_max; i > x_min; --i) {
        const auto pixel_up = bitmap.GetPixel(i, y - 1);
        if (pixel_up == fill_color) {
          continue;
        }
        if (first && pixel_up != wall) {
          s.push({i, y - 1});
          first = false;
          continue;
        }
        if (i + 1 == bitmap.GetWidth()) {
          if (pixel_up != wall) {
            s.push({i, y - 1});
          }
          continue;
        }

        const auto pixel_up_prev = bitmap.GetPixel(i + 1, y - 1);
        if (pixel_up_prev == wall && pixel_up != wall) {
          s.push({i, y - 1});
        }
      }
    }

    // find bottom switches
    first = true;
    if (y + 1 < bitmap.GetHeight()) {
      for (i = x_max; i > x_min; --i) {
        const auto pixel_down = bitmap.GetPixel(i, y + 1);
        if (pixel_down == fill_color) {
          continue;
        }

        if (first && pixel_down != wall) {
          s.push({i, y + 1});
          first = false;
        }
        if (i + 1 == bitmap.GetWidth()) {
          if (pixel_down != wall) {
            s.push({i, y + 1});
          }
          continue;
        }

        const auto pixel_down_prev = bitmap.GetPixel(i + 1, y + 1);
        if (pixel_down_prev == wall && pixel_down != wall) {
          s.push({i, y + 1});
        }
      }
    }
  }
}
#endif  // SCANLINEFILL_HPP
