#ifndef RANDOM_STUFF_HPP
#define RANDOM_STUFF_HPP

#include <map>
#include <set>

#include "../Bitmap/Bitmap.hpp"

void PolygonXORFill(Bitmap& bitmap,
                    std::vector<std::tuple<int, int, int, int>>& lines) {
  if (lines.empty()) {
    return;
  }

  for (auto [x1, y1, x2, y2] : lines) {
    if (y1 > y2) {
      std::swap(x1, x2);
      std::swap(y1, y2);
    }

    for (int y = y1; y < y2; ++y) {
      int x = x1 + (y - y1) * (x2 - x1) / (y2 - y1);
      for (int i = x + 1; i < bitmap.GetWidth(); ++i) {
        auto [r, g, b, t] = bitmap.GetPixel(i, y);

        if (r == 0 && g == 0 && b == 255) {
          continue;
        }

        r = (r == 255) ? 125 : 255;
        g = (g == 255) ? 197 : 255;
        b = (b == 255) ? 226 : 255;
        t = 255;
        bitmap.SetPixel(i, y, r, g, b, t);
      }
    }
  }
}

void PolygonFill(Bitmap& bitmap,
                 std::vector<std::tuple<int, int, int, int>>& lines) {
  if (lines.empty()) {
    return;
  }

  std::transform(
      lines.begin(), lines.end(), lines.begin(),
      [](std::tuple<int, int, int, int> t) -> std::tuple<int, int, int, int> {
        if (std::get<1>(t) < std::get<3>(t)) {
          return t;
        }
        return {std::get<2>(t), std::get<3>(t), std::get<0>(t), std::get<1>(t)};
      });

  std::sort(lines.begin(), lines.end(),
            [](std::tuple<int, int, int, int> lhv,
               std::tuple<int, int, int, int> rhv) -> bool {
              if (std::get<1>(lhv) < std::get<1>(rhv)) {
                return true;
              }
              return false;
            });

  auto getPointType = [](int y, int ay, int by) -> bool {
    bool a = (ay - y > 0);
    bool b = (by - y > 0);
    return a ^ b;
  };

  std::map<std::pair<int, int>, bool> point_types;
  for (size_t i = 0; i < lines.size(); ++i) {
    auto [ax1, ay1, ax2, ay2] = lines[i];
    for (size_t j = 0; j < lines.size(); ++j) {
      if (j == i) continue;
      auto [bx1, by1, bx2, by2] = lines[j];
      if (ax1 == bx1 && ay1 == by1) {
        point_types[{ax1, ay1}] = getPointType(ay1, ay2, by2);
      } else if (ax1 == bx2 && ay1 == by2) {
        point_types[{ax1, ay1}] = getPointType(ay1, ay2, by1);
      } else if (ax2 == bx1 && ay2 == by1) {
        point_types[{ax2, ay2}] = getPointType(ay2, ay1, by2);
      } else if (ax2 == bx2 && ay2 == by2) {
        point_types[{ax2, ay2}] = getPointType(ay2, ay1, by1);
      }
    }
  }

  size_t r = 0;
  size_t l = 0;

  for (int y = 0; y < bitmap.GetHeight(); ++y) {
    std::multiset<int> intersections;

    while (r < lines.size() && std::get<1>(lines[r]) <= y) {
      if (point_types[{std::get<0>(lines[r]), y}]) {
        intersections.insert(std::get<0>(lines[r]));
      }
      ++r;
    }
    while (l < lines.size() && std::get<3>(lines[l]) < y) {
      ++l;
    }

    for (size_t i = l; i < r; ++i) {
      const auto& [x1, y1, x2, y2] = lines[i];

      int x = x1 + (y - y1) * (x2 - x1) / (y2 - y1);
      if ((x1 <= x && x <= x2) || (x2 <= x && x <= x1)) {
        intersections.insert(x);
      }
    }

    if (intersections.size() % 2 != 0) {
      continue;
    }
    auto it = intersections.begin();
    while (it != intersections.end()) {
      int from = *it;
      ++it;
      int to = *it;
      ++it;
      for (int i = from + 1; i < to; ++i) {
        bitmap.SetPixel(i, y, 125, 197, 226, 255);
      }
    }
  }
}

#endif  // RANDOM_STUFF_HPP
