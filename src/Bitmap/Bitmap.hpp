#ifndef BITMAP_HPP
#define BITMAP_HPP

#include <SFML/Graphics/CircleShape.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <memory>

class Bitmap {
 public:
  explicit Bitmap(unsigned int w, unsigned int h, sf::RenderWindow* win)
      : width_(w), height_(h), window_(win) {
    pixels_ = std::unique_ptr<sf::Uint8[]>(new sf::Uint8[w * h * 4]);
    this->texture_.create(w, h);
    this->sprite_.setTexture(this->texture_);
  }

  inline void Update() {
    this->texture_.update(this->pixels_.get());
    this->window_->clear();
    this->window_->draw(sprite_);
  }

  inline void SetPixel(int x, int y, unsigned char red, unsigned char green,
                       unsigned char blue, unsigned char alpha) noexcept {
    if (x < 0 || static_cast<size_t>(x) >= this->width_ || y < 0 ||
        static_cast<size_t>(y) >= this->height_) {
      return;
    }

    std::swap(x, y);

    size_t index =
        (static_cast<size_t>(x) * this->width_ + static_cast<size_t>(y)) * 4;
    this->pixels_[index] = red;
    this->pixels_[index + 1] = green;
    this->pixels_[index + 2] = blue;
    this->pixels_[index + 3] = alpha;
  }

  [[nodiscard]] inline std::tuple<unsigned char, unsigned char, unsigned char,
                                  unsigned char>
  GetPixel(int x, int y) const {
    if (x < 0 || static_cast<size_t>(x) >= width_ || y < 0 ||
        static_cast<size_t>(y) >= height_) {
      throw std::invalid_argument("bad coordinates " + std::to_string(x) + " " +
                                  std::to_string(y));
    }

    std::swap(x, y);

    size_t index =
        (static_cast<size_t>(x) * width_ + static_cast<size_t>(y)) * 4;
    return {this->pixels_[index], this->pixels_[index + 1],
            this->pixels_[index + 2], this->pixels_[index + 3]};
  }

  inline void ClearPixels() noexcept {
    for (size_t i = 0; i < width_ * height_ * 4; i += 4) {
      this->pixels_[i] = 255;
      this->pixels_[i + 1] = 255;
      this->pixels_[i + 2] = 255;
      this->pixels_[i + 3] = 255;
    }
  }

  //[[nodiscard]] inline unsigned int GetWidth()const noexcept {return
  // this->width_;}
  //[[nodiscard]] inline unsigned int GetHeight() const noexcept {return
  // this->height_;}
  [[nodiscard]] inline int GetWidth() const noexcept {
    return static_cast<int>(this->width_);
  }
  [[nodiscard]] inline int GetHeight() const noexcept {
    return static_cast<int>(this->height_);
  }

 private:
  unsigned int width_;
  unsigned int height_;

  std::unique_ptr<sf::Uint8[]> pixels_;

  sf::RenderWindow* window_;
  sf::Texture texture_;
  sf::Sprite sprite_;
};

#endif  // BITMAP_HPP
