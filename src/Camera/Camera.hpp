#ifndef CAMERA_HPP
#define CAMERA_HPP

#include <limits>
#include <vector>

#include "../Matrices/Vector.hpp"
#include "../Transformations/Reflections.hpp"
#include "../Transformations/Resizes.hpp"
#include "../Transformations/Translations.hpp"

class Camera {
 public:
  Camera() = delete;

  explicit Camera(unsigned long long w, unsigned long long h)
      : width_(w), height_(h) {}

  [[nodiscard]] inline Vector<4> MakeProjection(Vector<4> v) const {
    if (this->perspectiveEnabled) {
      SquareMatrix<4> perspective_projection_matrix({{
          {1, 0, 0, 0},
          {0, 1, 0, 0},
          {0, 0, 1, this->perspectiveCoefficient},
          {0, 0, 0, 1},
      }});
      v = perspective_projection_matrix * v;
      for (size_t i = 0; i < 4; ++i) {
        v[i] /= v[3];
      }
    } else {
      static const SquareMatrix<4> orthogonal_projection_matrix({{
          {1, 0, 0, 0},
          {0, 1, 0, 0},
          {0, 0, 1, 0},
          {0, 0, 0, 1},
      }});
      v = orthogonal_projection_matrix * v;
    }
    return toScreen(v);
  }

  [[nodiscard]] inline Vector<4> toScreen(Vector<4> v) const {
    double xemax = static_cast<double>(this->width_);
    double xemin = 0;
    double yemax = static_cast<double>(this->height_);
    double yemin = 0;

    double xmax = xemax / 2.0;
    double xmin = -xmax;
    double ymax = yemax / 2.0;
    double ymin = -xmax;

    double kx = xmax - xmin == 0 ? std::numeric_limits<double>::infinity()
                                 : (xemax - xemin) / (xmax - xmin);
    double ky = ymax - ymin == 0 ? std::numeric_limits<double>::infinity()
                                 : (yemax - yemin) / (ymax - ymin);
    double k = std::min(kx, ky);
    k = k == std::numeric_limits<double>::infinity() ? 1 : k;
    v = Resize(v, k, k, k, 1);

    double dx = (xemin - k * xmin + xemax - k * xmax) / 2.0;
    double dy = (yemin - k * ymin + yemax - k * ymax) / 2.0;

    v = Translate(v, dx, dy, 0);

    return v;
  }

  bool perspectiveEnabled = false;
  double perspectiveCoefficient = -0.0001;

 private:
  unsigned long long width_;
  unsigned long long height_;
};

#endif  // CAMERA_HPP
