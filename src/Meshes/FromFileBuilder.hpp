#ifndef BUILDER_HPP
#define BUILDER_HPP

#include <spdlog/spdlog.h>

#include <fstream>
#include <string>

#include "Mesh.hpp"

class FromFileBuilder {
 public:
  [[nodiscard]] Mesh operator()(const std::string& path) {
    std::vector<Vector<4>> v;
    std::vector<std::array<size_t, 3>> f;

    spdlog::debug("read file from {}", path);
    std::ifstream fin(path);

    while (!fin.eof()) {
      std::string cmd;
      fin >> cmd;
      if (cmd == "v") {
        Vector<4> vertex;
        fin >> vertex[0] >> vertex[1] >> vertex[2];
        vertex[3] = 1;
        v.push_back(vertex);
      } else if (cmd == "f") {
        std::array<size_t, 3> face;
        fin >> face[0] >> face[1] >> face[2];
        for (auto& it : face) {
          --it;
        }
        f.push_back(face);
      }
    }
    fin.close();

    Vector<3> c{0, 0, 0};
    return Mesh(v, f, c);
  }
};

#endif  // BUILDER_HPP
