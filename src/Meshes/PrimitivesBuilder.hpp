#ifndef PRIMITIVEBUILDER_HPP
#define PRIMITIVEBUILDER_HPP

#include <cmath>
#include <vector>

#include "Mesh.hpp"

class PyramidBuilder {
 public:
  [[nodiscard]] Mesh operator()() {
    using std::sqrt;
    return Mesh(std::vector<Vector<4>>(
                    {Vector<4>({0, 0, 0, 1}), Vector<4>({1, 0, 0, 1}),
                     Vector<4>({0.5, sqrt(3) / 2, 0, 1}),
                     Vector<4>({0.5, sqrt(3) / 6, sqrt(2) / sqrt(6), 1})}),

                {{{0, 1, 2}, {0, 1, 3}, {1, 2, 3}, {2, 0, 3}}},

                Vector<3>({0, 0, 0}));
  }
};

class CubeBuilder {
 public:
  [[nodiscard]] Mesh operator()() {
    return Mesh(std::vector<Vector<4>>({
                    Vector<4>({-1, -1, 1, 1}),
                    Vector<4>({-1, 1, 1, 1}),
                    Vector<4>({1, 1, 1, 1}),
                    Vector<4>({1, -1, 1, 1}),
                    Vector<4>({-1, -1, -1, 1}),
                    Vector<4>({-1, 1, -1, 1}),
                    Vector<4>({1, 1, -1, 1}),
                    Vector<4>({1, -1, -1, 1}),
                }),

                {{{0, 1, 2},
                  {0, 2, 3},
                  {0, 3, 4},
                  {7, 4, 3},
                  {0, 5, 1},
                  {4, 5, 0},
                  {3, 2, 6},
                  {7, 3, 6},
                  {5, 2, 1},
                  {6, 2, 5},
                  {4, 6, 5},
                  {7, 6, 4}}},

                Vector<3>({0, 0, 0}));
  }
};

class OctahedronBuilder {
 public:
  [[nodiscard]] Mesh operator()() {
    return Mesh(std::vector<Vector<4>>(
                    {Vector<4>({0, 0, 1, 1}), Vector<4>({0, 0, -1, 1}),
                     Vector<4>({1, 1, 0, 1}), Vector<4>({-1, 1, 0, 1}),
                     Vector<4>({-1, -1, 0, 1}), Vector<4>({1, -1, 0, 1})}),

                {
                    {{0, 2, 3},
                     {0, 3, 4},
                     {0, 4, 5},
                     {0, 5, 2},
                     {1, 3, 2},
                     {1, 4, 3},
                     {1, 5, 4},
                     {1, 2, 5}},
                },
                Vector<3>({0, 0, 0}));
  }
};

class ISphericalBuilder {
 public:
  virtual ~ISphericalBuilder() = default;

 protected:
  virtual Vertex getVertex(double longitude, double latitude) = 0;

  std::pair<Vertices, Faces> generate(size_t longitudes, size_t latitudes) {
    Vertices vertices;
    Faces faces;

    double latitude_step = 360.0 / static_cast<double>(latitudes);
    double longitude_step = 360.0 / static_cast<double>(longitudes);

    for (double i = 0; i <= 360; i += longitude_step) {
      for (double j = 0; j <= 360; j += latitude_step) {
        vertices.push_back(getVertex(i, j));
      }
    }

    for (size_t i = 0; i + latitudes + 2 < vertices.size(); ++i) {
      faces.push_back({i, i + 1, i + latitudes + 1});
      faces.push_back({i + 1, i + latitudes + 2, i + latitudes + 1});
    }

    return {vertices, faces};
  }

  static double toRadians(double angle) { return M_PI / 180 * angle; }
};

class SphereV1Builder : ISphericalBuilder {
 public:
  SphereV1Builder(size_t longitudes, size_t latitudes, double radius)
      : longitudes_(longitudes), latitudes_(latitudes), radius_(radius) {}

  [[nodiscard]] Mesh operator()() {
    auto [v, f] = generate(longitudes_, latitudes_);
    return Mesh(v, f);
  }

 protected:
  [[nodiscard]] Vertex getVertex(double i, double j) override {
    double x = this->radius_ * sin(toRadians(i)) * cos(toRadians(j));
    double y = this->radius_ * sin(toRadians(i)) * sin(toRadians(j));
    double z = this->radius_ * cos(toRadians(i));

    return Vertex({x, y, z, 1});
  }

 private:
  size_t longitudes_;
  size_t latitudes_;
  double radius_;
};

class SphereV2Builder {
 public:
  SphereV2Builder(double radius, size_t iterations)
      : radius_(radius), iterations_(iterations){};

  [[nodiscard]] Mesh operator()() {
    Vertices vertices{Vertex({radius_, radius_, -radius_, 1}),
                      Vertex({radius_, -radius_, radius_, 1}),
                      Vertex({-radius_, -radius_, -radius_, 1}),
                      Vertex({-radius_, radius_, radius_, 1})};

    Faces faces = {{0, 1, 2}, {0, 1, 3}, {1, 2, 3}, {2, 0, 3}};

    for (size_t i = 0; i < iterations_; ++i) {
      Faces buf;

      for (const auto& face : faces) {
        const auto a = (vertices[face[0]] + vertices[face[1]]) / 2;
        const auto b = (vertices[face[1]] + vertices[face[2]]) / 2;
        const auto c = (vertices[face[0]] + vertices[face[2]]) / 2;

        vertices.push_back(a);
        vertices.push_back(b);
        vertices.push_back(c);

        size_t size = vertices.size();
        buf.push_back({size - 3, size - 2, size - 1});
        buf.push_back({face[0], size - 3, size - 1});
        buf.push_back({size - 3, face[1], size - 2});
        buf.push_back({size - 1, size - 2, face[2]});
      }

      faces.reserve(faces.size() + buf.size());
      faces.insert(faces.end(), buf.begin(), buf.end());
    }

    std::transform(vertices.begin(), vertices.end(), vertices.begin(),
                   [this](Vertex v) {
                     auto buf = Vector<3>({v.x(), v.y(), v.z()});
                     v = v / buf.l2() * this->radius_;
                     v[3] = 1;
                     return v;
                   });

    return Mesh(vertices, faces);
  }

 private:
  double radius_;
  size_t iterations_;
};

class TorusBuilder : ISphericalBuilder {
 public:
  TorusBuilder(size_t longitudes, size_t latitudes, double inner_radius, double outer_radius)
      : longitudes_(longitudes), latitudes_(latitudes), inner_radius_(inner_radius), outer_radius_(outer_radius) {}

  [[nodiscard]] Mesh operator()() {
    auto [v, f] = generate(longitudes_, latitudes_);
    return Mesh(v, f);
  }

 protected:
  [[nodiscard]] Vertex getVertex(double i, double j) override {
    double x = (outer_radius_ + inner_radius_ * sin(toRadians(i))) * cos(toRadians(j));
    double y = (outer_radius_ + inner_radius_ * sin(toRadians(i))) * sin(toRadians(j));
    double z = inner_radius_ * cos(toRadians(i));

    return Vertex({x, y, z, 1});
  }

 private:
  size_t longitudes_;
  size_t latitudes_;
  double inner_radius_;
  double outer_radius_;
};

class GarlicBuilder : ISphericalBuilder {
 public:
  GarlicBuilder(size_t longitudes, size_t latitudes, double radius)
      : longitudes_(longitudes), latitudes_(latitudes), radius_(radius) {}

  [[nodiscard]] Mesh operator()() {
    auto [v, f] = generate(longitudes_, latitudes_);
    return Mesh(v, f);
  }

 protected:
  [[nodiscard]] Vertex getVertex(double i, double j) override {
    double x = this->radius_ * cos(toRadians(i)) * sin(toRadians(j));
    double y = this->radius_ * cos(toRadians(i)) * cos(toRadians(j));
    double z = this->radius_ * sin(toRadians(i));

    x = x * (1 + 0.5 * fabs(sin(toRadians(2 * j))));
    y = y * (1 + 0.5 * fabs(sin(toRadians(2 * j))));

    if (i > 0 && i <= 90) {
      z = z + this->radius_ * pow(i / 90, 5);
    }

    return Vertex({x, y, z, 1});
  }

 private:
  size_t longitudes_;
  size_t latitudes_;
  double radius_;
};

#endif  // PRIMITIVEBUILDER_HPP
