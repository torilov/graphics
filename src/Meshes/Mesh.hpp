#ifndef MESH_HPP
#define MESH_HPP

#include <array>
#include <execution>
#include <future>
#include <iomanip>
#include <optional>
#include <vector>

#include "../Bitmap/Bitmap.hpp"
#include "../Camera/Camera.hpp"
#include "../Matrices/SquareMatrix.hpp"
#include "../Matrices/Vector.hpp"
#include "../Primitives/BresenhamLine.hpp"
#include "../Transformations/Reflections.hpp"
#include "../Transformations/Rotations.hpp"
#include "../Transformations/Translations.hpp"

using ZBuffer = std::optional<std::vector<std::vector<double>>>;
using Vertex = Vector<4>;
using Vertices = std::vector<Vertex>;
using Face = std::array<size_t, 3>;
using Faces = std::vector<Face>;
using Normal = Vector<3>;
using Normals = std::vector<Normal>;
using Coordinates = Vector<3>;

class Mesh {
 public:
  Mesh() = delete;
  explicit Mesh(Vertices v, Faces f, Vector<3> c = Vector<3>({0, 0, 0}))
      : vertices_(std::move(v)), faces_(std::move(f)), coordinates_(c) {
    this->ResetTransformation();
  };

  void Draw(Bitmap& bitmap, const Camera& camera, double angle, int effect,
            bool remove_invisible, ZBuffer& z_buffer) {
    Vertices transformed_vertices(this->vertices_.size());
    auto normals = (remove_invisible || z_buffer.has_value())
                       ? std::optional<Normals>(this->faces_.size())
                       : std::nullopt;
    auto visible_faces =
        remove_invisible ? std::optional<std::vector<char>>(this->faces_.size())
                         : std::nullopt;

    constexpr size_t n_cpus = 4;
    std::vector<std::future<void>> futures_vertices(n_cpus);
    for (size_t i = 0; i < n_cpus; ++i) {
      size_t index_begin = i * transformed_vertices.size() / 4;
      size_t index_end = (i + 1) * transformed_vertices.size() / 4;
      futures_vertices[i] = std::async(
          std::launch::async,
          [this, camera, angle, effect, &transformed_vertices](
              size_t begin, size_t end) mutable {
            for (size_t j = begin; j < end; ++j) {
              Vector<4> v = this->vertices_[j];
              v = this->self_transform_matrix_ * v;
              switch (effect) {
                case 0:
                  /* None */
                  v = Translate(v, this->coordinates_);
                  break;
                case 1:
                  /* Gyroscope */
                  v = RotateOy(v, angle);
                  v = Translate(v, this->coordinates_);
                  break;
                case 2:
                  /* Carousel */
                  v = Translate(v, this->coordinates_);
                  v = RotateOy(v, angle);
                  break;
                case 3:
                  /* Combo */
                  v = RotateOy(v, angle);
                  v = Translate(v, this->coordinates_);
                  v = RotateOy(v, angle);
                  break;
              }
              v = camera.MakeProjection(v);
              transformed_vertices[j] = v;
            }
          },
          index_begin, index_end);
    }
    for (auto& it : futures_vertices) {
      it.wait();
    }

    // center
    Vertex buf({this->coordinates_[0], this->coordinates_[1],
                this->coordinates_[2], 1});
    buf = camera.MakeProjection(buf);
    Coordinates center({buf[0], buf[1], buf[2]});

    if (normals.has_value()) {
      std::vector<std::future<void>> futures_faces(n_cpus);
      for (size_t i = 0; i < n_cpus; ++i) {
        size_t index_begin = i * this->faces_.size() / 4;
        size_t index_end = (i + 1) * this->faces_.size() / 4;
        futures_faces[i] = std::async(
            std::launch::async,
            [this, transformed_vertices, &normals, &visible_faces, center](
                size_t begin, size_t end) mutable {
              for (size_t j = begin; j < end; ++j) {
                auto normal =
                    faceNormal(faces_[j], transformed_vertices, center);
                normals.value()[j] = normal;
                if (visible_faces.has_value()) {
                  visible_faces.value()[j] = isFaceVisibleRoberts(normal);
                }
              }
            },
            index_begin, index_end);
      }
      for (auto& it : futures_faces) {
        it.wait();
      }
    }
    this->draw(bitmap, visible_faces, transformed_vertices, z_buffer, normals);
  }

  void ResetTransformation() {
    self_transform_matrix_ = SquareMatrix<4>({{
        {1, 0, 0, 0},
        {0, 1, 0, 0},
        {0, 0, 1, 0},
        {0, 0, 0, 1},
    }});
  }

  void AddTransformation(SquareMatrix<4> t) {
    self_transform_matrix_ = self_transform_matrix_ * t;
  }

  void setCoordinates(Coordinates coordinates) {
    this->coordinates_ = coordinates;
  }

  void setKd(double kd) { this->kd_ = kd; }

  SquareMatrix<4> self_transform_matrix_;

  void ExportToFile(const std::string& path) {
    spdlog::debug("save to file {}", path);
    std::ofstream fout(path);

    for (const auto& vertex : vertices_) {
      fout << 'v' << std::fixed << std::setprecision(2) << ' ' << vertex.x()
           << ' ' << vertex.y() << ' ' << vertex.z() << '\n';
    }

    for (const auto& face : faces_) {
      fout << 'f' << ' ' << face[0] + 1 << ' ' << face[1] + 1 << ' '
           << face[2] + 1 << '\n';
    }

    fout.close();
  }

 private:
  void draw(Bitmap& bitmap, std::optional<std::vector<char>> visible_faces,
            const Vertices& transformed_vertices, ZBuffer& z_buffer,
            std::optional<Normals> normals) {
    for (size_t i = 0; i < this->faces_.size(); ++i) {
      if (visible_faces.has_value() && visible_faces.value()[i] == false) {
        continue;
      }

      const Face& face = faces_[i];

      Vertex a = transformed_vertices[face[0]];
      Vertex b = transformed_vertices[face[1]];
      Vertex c = transformed_vertices[face[2]];

      if (!z_buffer.has_value()) {
        BresenhamLine(bitmap, static_cast<int>(a[0]), static_cast<int>(a[1]),
                      static_cast<int>(b[0]), static_cast<int>(b[1]));
        BresenhamLine(bitmap, static_cast<int>(a[0]), static_cast<int>(a[1]),
                      static_cast<int>(c[0]), static_cast<int>(c[1]));
        BresenhamLine(bitmap, static_cast<int>(b[0]), static_cast<int>(b[1]),
                      static_cast<int>(c[0]), static_cast<int>(c[1]));
        continue;
      }

      drawFaceWithZBuffer(bitmap, a, b, c, normals.value()[i], z_buffer);
    }
  }

  inline void drawFaceWithZBuffer(Bitmap& bitmap, Vertex a, Vertex b, Vertex c,
                                  const Normal& normal,
                                  ZBuffer& z_buffer) const {
    if (a.y() > b.y()) {
      std::swap(a, b);
    }
    if (a.y() > c.y()) {
      std::swap(a, c);
    }
    if (b.y() > c.y()) {
      std::swap(b, c);
    }

    if (a.y() == b.y()) {
      fillFlatTopTriangle(bitmap, a, b, c, normal, z_buffer);
    } else if (b.y() == c.y()) {
      fillFlatBottomTriangle(bitmap, a, b, c, normal, z_buffer);
    } else {
      Vector<4> d({
          a.x() + (b.y() - a.y()) * (c.x() - a.x()) / (c.y() - a.y()),
          b.y(),
          a.z() + (c.z() - a.z()) * (b.y() - a.y()) / (c.y() - a.y()),
          1,
      });
      fillFlatTopTriangle(bitmap, b, d, c, normal, z_buffer);
      fillFlatBottomTriangle(bitmap, a, b, d, normal, z_buffer);
    }
  }

  inline void fillFlatBottomTriangle(Bitmap& bitmap, Vertex a, Vertex b,
                                     Vertex c, const Normal& normal,
                                     ZBuffer& z_buffer) const {
    if (b.x() > c.x()) {
      std::swap(b, c);
    }

    double step_x_left = (b.x() - a.x()) / (b.y() - a.y());
    double step_x_right = (c.x() - a.x()) / (c.y() - a.y());

    double step_z_left = (b.z() - a.z()) / (b.y() - a.y());
    double step_z_right = (c.z() - a.z()) / (c.y() - a.y());

    double x_left = a.x();
    double x_right = a.x();

    double z_left = a.z();
    double z_right = a.z();

    for (int y = static_cast<int>(a.y()); y <= static_cast<int>(b.y()); ++y) {
      for (double x = x_left; x <= x_right; x += 0.1) {
        if (x < 0 || x >= 1920 || y < 0 || y >= 1080) continue;
        if (x < std::min(b.x(), a.x())) continue;
        if (x > std::max(c.x(), a.x())) continue;
        double z =
            z_left + (x - x_left) * (z_right - z_left) / (x_right - x_left);
        if (z_buffer.value()[static_cast<unsigned long>(y)]
                            [static_cast<unsigned long>(x)] < z) {
          z_buffer.value()[static_cast<unsigned long>(y)]
                          [static_cast<unsigned long>(x)] = z;

          double intensity_ =
              intensity(normal, Vertex({x, static_cast<double>(y), z, 1}));
          auto color_ = color(0, 0, 255, intensity_);

          bitmap.SetPixel(static_cast<int>(x), y, color_[0], color_[1],
                          color_[2], 255);
        }
      }

      x_left += step_x_left;
      x_right += step_x_right;

      z_left += step_z_left;
      z_right += step_z_right;
    }
  }

  inline void fillFlatTopTriangle(Bitmap& bitmap, Vertex a, Vertex b, Vertex c,
                                  const Normal& normal,
                                  ZBuffer& z_buffer) const {
    if (a.x() > b.x()) {
      std::swap(a, b);
    }

    double step_x_left = (c.x() - a.x()) / (c.y() - a.y());
    double step_x_right = (c.x() - b.x()) / (c.y() - b.y());

    double step_z_left = (c.z() - a.z()) / (c.y() - a.y());
    double step_z_right = (c.z() - b.z()) / (c.y() - b.y());

    double x_left = c.x();
    double x_right = c.x();

    double z_left = c.z();
    double z_right = c.z();

    for (int y = static_cast<int>(c.y()); y >= static_cast<int>(a.y()); --y) {
      for (double x = x_left; x <= x_right; x += 0.1) {
        if (x < 0 || x >= 1920 || y < 0 || y >= 1080) continue;
        if (x < std::min(a.x(), c.x())) continue;
        if (x > std::max(b.x(), c.x())) continue;
        double z =
            z_left + (x - x_left) * (z_right - z_left) / (x_right - x_left);
        if (z_buffer.value()[static_cast<unsigned long>(y)]
                            [static_cast<unsigned long>(x)] < z) {
          z_buffer.value()[static_cast<unsigned long>(y)]
                          [static_cast<unsigned long>(x)] = z;

          double intensity_ =
              intensity(normal, Vertex({x, static_cast<double>(y), z, 1}));
          auto color_ = color(0, 0, 255, intensity_);

          bitmap.SetPixel(static_cast<int>(x), y, color_[0], color_[1],
                          color_[2], 255);
        }
      }

      x_left -= step_x_left;
      x_right -= step_x_right;

      z_left -= step_z_left;
      z_right -= step_z_right;
    }
  }

  static inline std::array<unsigned char, 3> color(unsigned char r,
                                                   unsigned char g,
                                                   unsigned char b,
                                                   double intensity) {
    return {
        static_cast<unsigned char>(
            std::max(std::min(r * intensity, 255.0), 0.0)),
        static_cast<unsigned char>(
            std::max(std::min(g * intensity, 255.0), 0.0)),
        static_cast<unsigned char>(
            std::max(std::min(b * intensity, 255.0), 0.0)),
    };
  }

  [[nodiscard]] inline double intensity(const Normal& normal,
                                        const Vertex& v) const {
    constexpr double ia = 2;
    constexpr double ka = 0.25;
    constexpr double il = 1.175;
    constexpr double k = 0.7;
    constexpr double ks = 0.125;
    constexpr int power = 10;

    Vertex light_source({1080 / 2, 1920 / 2, 500, 1});

    Vector<3> norm({normal.x(), normal.y(), normal.z()});
    norm /= norm.l2();

    Vector<3> ray({v.x() - light_source.x(), v.y() - light_source.y(),
                   v.z() - light_source.z()});
    ray /= ray.l2();
    double distance = ray.l2();

    double O = Vector<3>::scalarProduct(norm, ray);

    Vector<3> reflection({2 * normal.z() * normal.x(),
                          2 * normal.z() * normal.y(),
                          2 * normal.z() * normal.z() - 1});
    reflection /= reflection.l2();

    double b = Vector<3>::scalarProduct(reflection, ray);

    return ia * ka + il / (distance + k) * (this->kd_ * O + ks * pow(b, power));
  }

  [[nodiscard]] static inline bool isFaceVisibleRoberts(const Normal& normal) {
    return Vector<3>::scalarProduct(normal, Vector<3>({0, 0, 1})) < 0;
  }

  [[nodiscard]] static inline Normal faceNormal(const Face& face,
                                                const Vertices& vertices,
                                                const Coordinates& center) {
    const Vertex& a = vertices[face[0]];
    const Vertex& b = vertices[face[1]];
    const Vertex& c = vertices[face[2]];

    const Vertex v1 = b - a;
    const Vertex v2 = c - a;

    const auto v = Vector<3>::vectorProduct(Vector<3>({v1[0], v1[1], v1[2]}),
                                            Vector<3>({v2[0], v2[1], v2[2]}));

    double coefficient_a = v.x();
    double coefficient_b = v.y();
    double coefficient_c = v.z();
    double coefficient_d =
        -Vector<3>::scalarProduct(v, Vector<3>{a[0], a[1], a[2]});

    if (coefficient_a * center.x() + coefficient_b * center.y() +
            coefficient_c * center.z() + coefficient_d <
        0) {
      coefficient_a *= -1;
      coefficient_b *= -1;
      coefficient_c *= -1;
      coefficient_d *= -1;
    }

    return Normal{coefficient_a, coefficient_b, coefficient_c};
  }

  Vertices vertices_;
  Faces faces_;
  Coordinates coordinates_;
  double kd_ = 0.6;
};

#endif  // MESH_HPP
