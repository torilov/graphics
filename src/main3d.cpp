// Torilov Dmitry
// BSE 173
// Linux, C++17, CMake, CONAN

#include <imgui-SFML.h>
#include <imgui.h>
#include <spdlog/spdlog.h>

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/System/Clock.hpp>
#include <SFML/Window/Event.hpp>
#include <array>
#include <string>
#include <vector>

#include "Bitmap/Bitmap.hpp"
#include "Meshes/FromFileBuilder.hpp"
#include "Meshes/Mesh.hpp"
#include "Meshes/PrimitivesBuilder.hpp"

// Constant window shape
constexpr int width = 1920;
constexpr int height = 1080;

int main() {
  // Init spdlog
  spdlog::set_level(spdlog::level::debug);
  spdlog::info("Starting ImGui + SFML");

  // Main Window initial
  sf::RenderWindow window(sf::VideoMode(width, height), "SFML");
  window.setFramerateLimit(60);
  ImGui::SFML::Init(window);
  sf::Mouse::setPosition(sf::Vector2i(100, 200), window);

  Bitmap bitmap(width, height, &window);
  bitmap.ClearPixels();

  Camera camera(width, height);

  double angle = 0;
  std::vector<std::tuple<std::string, Vector<3>, Vector<3>, Vector<3>, int,
                         bool, bool, bool, double, std::unique_ptr<Mesh>>>
      meshes;

  std::unique_ptr<Mesh> sphere_v1 =
      std::make_unique<Mesh>(SphereV1Builder(36, 18, 1)());
  meshes.emplace_back("Sphere v1", Vector<3>(), Vector<3>(),
                      Vector<3>({100, 100, 100}), 0, true, false, true, 0.6,
                      std::move(sphere_v1));

  std::unique_ptr<Mesh> sphere_v2 =
      std::make_unique<Mesh>(SphereV2Builder(1, 4)());
  meshes.emplace_back("Sphere v2", Vector<3>(), Vector<3>(),
                      Vector<3>({100, 100, 100}), 0, false, false, false, 0.6,
                      std::move(sphere_v2));

  std::unique_ptr<Mesh> torus =
      std::make_unique<Mesh>(TorusBuilder(36, 18, 1, 2)());
  meshes.emplace_back("Torus", Vector<3>(), Vector<3>(),
                      Vector<3>({100, 100, 100}), 0, false, false, false, 0.6,
                      std::move(torus));

  std::unique_ptr<Mesh> garlic =
      std::make_unique<Mesh>(GarlicBuilder(36, 18, 1)());

  garlic->ExportToFile("/home/dim/garlic.obj");

  meshes.emplace_back("Garlic", Vector<3>(), Vector<3>(),
                      Vector<3>({100, 100, 100}), 0, false, false, false, 0.6,
                      std::move(garlic));

  std::unique_ptr<Mesh> cube = std::make_unique<Mesh>(CubeBuilder()());
  meshes.emplace_back("Cube", Vector<3>(), Vector<3>(),
                      Vector<3>({100, 100, 100}), 0, false, true, false, 0.6,
                      std::move(cube));

  std::unique_ptr<Mesh> pyramid = std::make_unique<Mesh>(PyramidBuilder()());
  meshes.emplace_back("Pyramid", Vector<3>(), Vector<3>(), Vector<3>({1, 1, 1}),
                      0, false, false, false, 0.6, std::move(pyramid));

  std::unique_ptr<Mesh> cat = std::make_unique<Mesh>(
      FromFileBuilder()("/home/dim/Documents/lowpolycat/cat4.obj"));
  meshes.emplace_back("Cat", Vector<3>(), Vector<3>(), Vector<3>({1, 1, 1}), 0,
                      false, false, true, 0.6, std::move(cat));

  std::unique_ptr<Mesh> duck = std::make_unique<Mesh>(
      FromFileBuilder()("/home/dim/Documents/duck/ducky_triangulated2.obj"));
  meshes.emplace_back("Duck", Vector<3>(), Vector<3>(), Vector<3>({1, 1, 1}), 0,
                      false, false, false, 0.6, std::move(duck));

  ZBuffer z_buffer((std::vector<std::vector<double>>(height)));
  ZBuffer no_z_buffer;
  for (auto& it : z_buffer.value()) {
    it.resize(width);
  };

  // ---------------------------- ImGUI Settings ------------------------------
  float imgui_perspective_coefficient = 1000;
  bool imgui_perspective_enabled = true;

  // ------------------------------- Main Loop --------------------------------
  sf::Clock deltaClock;
  while (window.isOpen()) {
    // Handle window close
    sf::Event event;
    while (window.pollEvent(event)) {
      ImGui::SFML::ProcessEvent(event);

      if (event.type == sf::Event::Closed) {
        window.close();
      }
    }

    // ---------------------------- Drop z-buffer ------------------------------
    for (auto& it : z_buffer.value()) {
      for (auto& jt : it) {
        jt = -600;
      }
    }

    // ---------------------------- Draw Figures ------------------------------
    bitmap.ClearPixels();

    angle += 0.01;
    for (auto& [name, coordinates, angles, resizes, effect, show,
                remove_invisible, enable_z_buffer, kd, mesh] : meshes) {
      if (show) {
        mesh->ResetTransformation();
        mesh->self_transform_matrix_ =
            RotateOx(mesh->self_transform_matrix_, angles[0]);
        mesh->self_transform_matrix_ =
            RotateOy(mesh->self_transform_matrix_, angles[1]);
        mesh->self_transform_matrix_ =
            RotateOz(mesh->self_transform_matrix_, angles[2]);
        mesh->self_transform_matrix_ =
            Resize(mesh->self_transform_matrix_, resizes[0], resizes[1],
                   resizes[2], 1);
        mesh->setCoordinates(coordinates);
        mesh->setKd(kd);

        mesh->Draw(bitmap, camera, angle, effect, remove_invisible,
                   enable_z_buffer ? z_buffer : no_z_buffer);
      }
    }

    // ------------------------- Update menu window ---------------------------
    ImGui::SFML::Update(window, deltaClock.restart());

    ImGui::Begin("Control Panel");
    ImGui::SetWindowFontScale(2);

    ImGui::Checkbox("perspective enabled", &imgui_perspective_enabled);
    camera.perspectiveEnabled = imgui_perspective_enabled;

    ImGui::SliderFloat("perspective coefficient",
                       &imgui_perspective_coefficient, 800, 2000);
    camera.perspectiveCoefficient =
        static_cast<double>(-1 / imgui_perspective_coefficient);

    for (auto& [name, coordinates, angles, resizes, effects, show,
                remove_invisible, enable_z_buffer, kd, mesh] : meshes) {
      ImGui::Checkbox((name + " enabled").c_str(), &show);
      ImGui::Checkbox((name + " remove invisible").c_str(), &remove_invisible);
      ImGui::Checkbox((name + " enable z-buffer").c_str(), &enable_z_buffer);

      auto x = static_cast<float>(coordinates[0]);
      auto y = static_cast<float>(coordinates[1]);
      auto z = static_cast<float>(coordinates[2]);
      ImGui::SliderFloat((name + " x coordinate").c_str(), &x, -800, 800);
      ImGui::SliderFloat((name + " y coordinate").c_str(), &y, -800, 800);
      ImGui::SliderFloat((name + " z coordinate").c_str(), &z, -800, 800);
      coordinates[0] = static_cast<double>(x);
      coordinates[1] = static_cast<double>(y);
      coordinates[2] = static_cast<double>(z);

      auto kd_ = static_cast<float>(kd);
      ImGui::SliderFloat((name + " kd").c_str(), &kd_, 0, 2);
      kd = static_cast<double>(kd_);

      auto angle_x = static_cast<float>(angles[0] * 180 / 3.14);
      auto angle_y = static_cast<float>(angles[1] * 180 / 3.14);
      auto angle_z = static_cast<float>(angles[2] * 180 / 3.14);
      ImGui::SliderFloat((name + " x angle").c_str(), &angle_x, -360, 360);
      ImGui::SliderFloat((name + " y angle").c_str(), &angle_y, -360, 360);
      ImGui::SliderFloat((name + " z angle").c_str(), &angle_z, -360, 360);
      angles[0] = static_cast<double>(angle_x) * 3.14 / 180;
      angles[1] = static_cast<double>(angle_y) * 3.14 / 180;
      angles[2] = static_cast<double>(angle_z) * 3.14 / 180;

      auto resize_x = static_cast<float>(resizes[0]);
      auto resize_y = static_cast<float>(resizes[1]);
      auto resize_z = static_cast<float>(resizes[2]);
      ImGui::SliderFloat((name + " x resize").c_str(), &resize_x, 0, 100);
      ImGui::SliderFloat((name + " y resize").c_str(), &resize_y, 0, 100);
      ImGui::SliderFloat((name + " z resize").c_str(), &resize_z, 0, 100);
      resizes[0] = static_cast<double>(resize_x);
      resizes[1] = static_cast<double>(resize_y);
      resizes[2] = static_cast<double>(resize_z);

      static const int effects_number = 4;
      static const char* effects_list[effects_number]{
          "None", "Gyroscope", "Carousel", "Combination"};
      ImGui::ListBox((name + " effect").c_str(), &effects, effects_list,
                     effects_number);
    }

    if (ImGui::Button("About")) {
      spdlog::info(
          "C++/SFML/ImGui app. Build with CMake and CONAN. "
          "Written by Torilov Dmitry. "
          "Uses initial C++ repo by Jason Turner.");
    }
    if (ImGui::Button("Description")) {
      spdlog::info("Use panel to change rendering parameters.");
    }
    ImGui::End();

    bitmap.Update();

    ImGui::SFML::Render(window);
    window.display();
  }

  ImGui::SFML::Shutdown();

  return 0;
}
